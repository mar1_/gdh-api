<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDedicatedPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dedicated_pages', function (Blueprint $table) {
            $table->increments('id');

            $table->string('seo_title');
            $table->text('seo_meta');
            $table->text('seo_main');
            $table->string('seo_short');
            $table->string('seo_url');

            $table->integer('com_bill')->unique()->nullable();
            $table->text('com_comments');
            $table->text('com_contact_comments');
            $table->string('com_person');
            $table->date('com_date_alert');
            $table->date('com_date_start_contract');
            $table->date('com_date_end_contract');

            $table->integer('info_artisan_id')->unique()->nullable();
            $table->text('info_description');
            $table->text('info_activities')->nullable();
            $table->text('info_activities_detailed')->nullable();
            $table->text('info_labels')->nullable();
            $table->text('info_departments');
            $table->string('info_gender')->nullable();
            $table->string('info_first_name')->nullable();
            $table->string('info_last_name')->nullable();
            $table->string('info_phone_one')->nullable();
            $table->string('info_phone_two')->nullable();
            $table->string('info_phone_mobile')->nullable();
            $table->date('info_register_date')->nullable();
            $table->string('info_siret')->nullable();
            $table->string('info_status')->nullable();
            $table->string('info_status_else')->nullable();
            $table->string('info_nb_employees')->nullable();
            $table->string('info_creation_year')->nullable();
            $table->string('info_address')->nullable();
            $table->string('info_postal')->nullable();
            $table->string('info_city')->nullable();
            $table->string('info_mail')->nullable();
            $table->string('info_mail_estimate')->nullable();
            $table->boolean('info_indexed')->nullable();
            $table->boolean('info_calltracking')->nullable();
            $table->boolean('info_estimate')->nullable();
            $table->boolean('info_estimate_limit')->nullable();
            $table->boolean('info_page')->nullable();
            $table->boolean('info_chart_signer')->nullable();
            $table->boolean('info_expo')->nullable();
            $table->boolean('info_coop')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dedicated_pages');
    }
}
