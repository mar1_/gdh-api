<?php

use Illuminate\Database\Seeder;
use App\DedicatedPage;

class DedicatedPagesTableSeeder extends Seeder
{
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        DedicatedPage::truncate();

        $faker = \Faker\Factory::create('fr_FR');

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            DedicatedPage::create([
                'seo_title' => $faker->company,
                'seo_meta' => $faker->paragraph,
                'seo_main' => $faker->sentence,
                'seo_short' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'seo_url' => $faker->url,
                'com_comments' => $faker->paragraph,
                'com_contact_comments' => $faker->paragraph,
                'com_person' => $faker->name,
                'com_date_alert' => $faker->dateTime($max = 'now', $timezone = 'Europe/Paris'),
                'com_date_start_contract' => $faker->dateTime($max = 'now', $timezone = 'Europe/Paris'),
                'com_date_end_contract' => $faker->dateTime($max = 'now', $timezone = 'Europe/Paris'),
                'info_description' => $faker->paragraph,
                'info_register_date' => $faker->dateTime($max = 'now', $timezone = 'Europe/Paris'),
                'info_first_name' => $faker->firstName,
                'info_last_name' => $faker->lastName,
                'info_address' => $faker->address,
                'info_postal' => $faker->postcode,
                'info_city' => $faker->city,
                'info_departments' => $faker->departmentName,
                'info_mail' => $faker->email,
                'info_mail_estimate' => $faker->email,
                'info_phone_one' => $faker->phoneNumber,
                'info_phone_mobile' => $faker->mobileNumber,
                'info_siret' => $faker->siret,
            ]);
        }
    }
}
