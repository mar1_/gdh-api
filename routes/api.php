<?php

use Illuminate\Http\Request;
Use App\Article;


// Authentification
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');
Route::post('register', 'AuthController@register');


// Utilisateurs
Route::get('users', 'UserController@index');
Route::post('users/update/{user}', 'UserController@update');
Route::delete('users/{user}', 'UserController@delete');


// Articles
Route::get('articles', 'ArticleController@index');
Route::get('articles/{article}', 'ArticleController@show');
Route::post('articles', 'ArticleController@store');
Route::put('articles/{article}', 'ArticleController@update');
Route::delete('articles/{article}', 'ArticleController@delete');

// Debug
Route::get('debugs', 'DebugController@index');
Route::get('debugs/{debug}', 'DebugController@show');
Route::post('debugs', 'DebugController@store');
Route::put('debugs/{debug}', 'DebugController@update');
Route::delete('debugs/{debug}', 'DebugController@delete');

// Pages dédiées
Route::get('pages', 'DedicatedPageController@index');
Route::delete('pages/{dedicatedPage}', 'DedicatedPageController@delete');
