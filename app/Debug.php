<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debug extends Model
{
    protected $fillable = ['user_name', 'bug_content', 'solved', 'solved_at'];
}
