<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DedicatedPage;

class DedicatedPageController extends Controller
{
  public function index()
  {
      return DedicatedPage::all();
  }

  public function delete(DedicatedPage $dedicatedPage)
  {
      $dedicatedPage->delete();
      return response()->json(null, 204);
  }
}
