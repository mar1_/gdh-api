<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Debug;

class DebugController extends Controller
{
  public function index()
  {
      return Debug::all();
  }

  public function show(Debug $debug)
  {
      return $debug;
  }

  public function store(Request $request)
  {
      $debug = Debug::create($request->all());

      return response()->json($debug, 201);
  }

  public function update(Request $request, Debug $debug)
  {
      $debug->update($request->all());

      return response()->json($debug, 200);
  }

  public function delete(Debug $debug)
  {
      $debug->delete();

      return response()->json(null, 204);
  }
}
