<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DedicatedPage extends Model
{
    protected $fillable = ['seo_title', 'info_description', 'info_departments'];
}
